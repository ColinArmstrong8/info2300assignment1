USE [master]
GO
/****** Object:  Database [CVGS]    Script Date: 2020-12-08 6:18:51 PM ******/
CREATE DATABASE [CVGS]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CVGS', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\CVGS.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'CVGS_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\CVGS_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [CVGS] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CVGS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CVGS] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CVGS] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CVGS] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CVGS] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CVGS] SET ARITHABORT OFF 
GO
ALTER DATABASE [CVGS] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CVGS] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CVGS] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CVGS] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CVGS] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CVGS] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CVGS] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CVGS] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CVGS] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CVGS] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CVGS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CVGS] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CVGS] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CVGS] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CVGS] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CVGS] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CVGS] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CVGS] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [CVGS] SET  MULTI_USER 
GO
ALTER DATABASE [CVGS] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CVGS] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CVGS] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CVGS] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [CVGS] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [CVGS] SET QUERY_STORE = OFF
GO
USE [CVGS]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AddressMailing]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AddressMailing](
	[mailingId] [uniqueidentifier] NOT NULL,
	[street] [nvarchar](20) NOT NULL,
	[postalCode] [nvarchar](6) NOT NULL,
	[city] [nvarchar](20) NOT NULL,
	[apartmentNumber] [nvarchar](20) NOT NULL,
	[firstName] [nvarchar](20) NOT NULL,
	[lastName] [nvarchar](20) NOT NULL,
	[CountryCode] [nvarchar](3) NULL,
	[lastModified] [datetime] NULL,
	[userId] [nvarchar](450) NOT NULL,
	[ProvinceCode] [nvarchar](2) NULL,
	[Archived] [bit] NULL,
 CONSTRAINT [PK__AddressM__4E8DFD561DD20E0B] PRIMARY KEY CLUSTERED 
(
	[mailingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AddressShipping]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AddressShipping](
	[shippingId] [uniqueidentifier] NOT NULL,
	[street] [nvarchar](20) NOT NULL,
	[postalCode] [nvarchar](6) NOT NULL,
	[city] [nvarchar](20) NOT NULL,
	[apartmentNumber] [nvarchar](20) NOT NULL,
	[firstName] [nvarchar](20) NOT NULL,
	[lastName] [nvarchar](20) NOT NULL,
	[CountryCode] [nvarchar](3) NULL,
	[lastModified] [datetime] NULL,
	[userId] [nvarchar](450) NOT NULL,
	[ProvinceCode] [nvarchar](2) NULL,
	[Archived] [bit] NULL,
 CONSTRAINT [PK__AddressS__EDF80BCA6656726E] PRIMARY KEY CLUSTERED 
(
	[shippingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](450) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](450) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[PromoEmailEnabled] [bit] NOT NULL,
	[Bio] [nvarchar](max) NOT NULL,
	[GamerTag] [nvarchar](max) NOT NULL,
	[firstName] [nvarchar](450) NULL,
	[lastName] [nvarchar](450) NULL,
	[dateOfBirth] [datetime] NULL,
	[gender] [nvarchar](450) NULL,
	[city] [nvarchar](450) NULL,
	[countryCode] [nvarchar](3) NULL,
	[provinceCode] [nvarchar](2) NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserTokens](
	[UserId] [nvarchar](450) NOT NULL,
	[LoginProvider] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CartItem]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CartItem](
	[userId] [nvarchar](450) NOT NULL,
	[gameId] [uniqueidentifier] NOT NULL,
	[GameFormatCode] [nvarchar](1) NOT NULL,
	[quantity] [int] NOT NULL,
	[lastModified] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[userId] ASC,
	[gameId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CategoryPreference]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoryPreference](
	[userId] [nvarchar](450) NOT NULL,
	[gamecategoryId] [int] NOT NULL,
	[lastModified] [datetime] NULL,
 CONSTRAINT [PK__Category__5503CE5E31DD18C6] PRIMARY KEY CLUSTERED 
(
	[userId] ASC,
	[gamecategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[Code] [nvarchar](3) NOT NULL,
	[Alpha2Code] [nvarchar](2) NOT NULL,
	[EnglishName] [nvarchar](50) NOT NULL,
	[FrenchName] [nvarchar](50) NOT NULL,
 CONSTRAINT [Country_PK] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EsrbRating]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EsrbRating](
	[Code] [nvarchar](5) NOT NULL,
	[EnglishRating] [nvarchar](30) NOT NULL,
	[FrenchRating] [nvarchar](30) NOT NULL,
 CONSTRAINT [EsrbRatingCode_PK] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Game]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Game](
	[Guid] [uniqueidentifier] NOT NULL,
	[GameFormatCode] [nvarchar](1) NOT NULL,
	[GameCategoryId] [int] NOT NULL,
	[GameSubCategoryId] [int] NULL,
	[EsrbRatingCode] [nvarchar](5) NOT NULL,
	[EnglishName] [nvarchar](70) NOT NULL,
	[FrenchName] [nvarchar](70) NOT NULL,
	[FrenchVersion] [bit] NOT NULL,
	[EnglishPlayerCount] [nvarchar](30) NULL,
	[FrenchPlayerCount] [nvarchar](30) NULL,
	[GamePerspectiveCode] [nvarchar](1) NULL,
	[EnglishTrailer] [nvarchar](4000) NULL,
	[FrenchTrailer] [nvarchar](4000) NULL,
	[EnglishDescription] [nvarchar](4000) NULL,
	[FrenchDescription] [nvarchar](4000) NULL,
	[EnglishDetail] [nvarchar](4000) NULL,
	[FrenchDetail] [nvarchar](4000) NULL,
	[UserName] [nvarchar](30) NOT NULL,
	[lastModified] [datetime] NULL,
	[Price] [float] NULL,
	[Archived] [bit] NULL,
 CONSTRAINT [Game_PK] PRIMARY KEY CLUSTERED 
(
	[Guid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GameCategory]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GameCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EnglishCategory] [nvarchar](20) NOT NULL,
	[FrenchCategory] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_GameCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GameFormat]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GameFormat](
	[Code] [nvarchar](1) NOT NULL,
	[EnglishCategory] [nvarchar](15) NOT NULL,
	[FrenchCategory] [nvarchar](15) NOT NULL,
 CONSTRAINT [GameStatus_PK] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GamePerspective]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GamePerspective](
	[Code] [nvarchar](1) NOT NULL,
	[EnglishPerspectiveName] [nvarchar](15) NOT NULL,
	[FrenchPerspectiveName] [nvarchar](15) NOT NULL,
 CONSTRAINT [GamePerspective_PK] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GameSubCategory]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GameSubCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GameCategoryId] [int] NOT NULL,
	[EnglishCategory] [nvarchar](20) NOT NULL,
	[FrenchCategory] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_GameSubCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[Id] [uniqueidentifier] NOT NULL,
	[userId] [nvarchar](450) NOT NULL,
	[shippingId] [uniqueidentifier] NULL,
	[mailingId] [uniqueidentifier] NULL,
	[isShipped] [bit] NULL,
	[dateCreated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderItem]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderItem](
	[orderId] [uniqueidentifier] NOT NULL,
	[gameId] [uniqueidentifier] NOT NULL,
	[GameFormatCode] [nvarchar](1) NOT NULL,
	[quantity] [int] NOT NULL,
	[lastModified] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[orderId] ASC,
	[gameId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Platform]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Platform](
	[Code] [nvarchar](10) NOT NULL,
	[EnglishName] [nvarchar](40) NOT NULL,
	[FrenchName] [nvarchar](40) NOT NULL,
 CONSTRAINT [Platform_PK] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PlatformPreference]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlatformPreference](
	[userId] [nvarchar](450) NOT NULL,
	[platformCode] [nvarchar](10) NOT NULL,
	[lastModified] [datetime] NULL,
 CONSTRAINT [PK__Platform__A669FBFE92BE477D] PRIMARY KEY CLUSTERED 
(
	[userId] ASC,
	[platformCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Province]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Province](
	[Code] [nvarchar](2) NOT NULL,
	[CountryCode] [nvarchar](3) NOT NULL,
	[EnglishName] [nvarchar](50) NOT NULL,
	[FrenchName] [nvarchar](50) NOT NULL,
	[FederalTax] [float] NULL,
	[FederalTaxAcronym] [nvarchar](10) NULL,
	[ProvincialTax] [float] NULL,
	[ProvincialTaxAcronym] [nvarchar](10) NULL,
	[PstOnGst] [bit] NULL,
 CONSTRAINT [ProvinceLookup_PK] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubCategoryPreference]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubCategoryPreference](
	[userId] [nvarchar](450) NOT NULL,
	[gameSubcategoryId] [int] NOT NULL,
	[lastModified] [datetime] NULL,
 CONSTRAINT [PK__SubCateg__AEDF96AEA68DA694] PRIMARY KEY CLUSTERED 
(
	[userId] ASC,
	[gameSubcategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserGameLibraryItem]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserGameLibraryItem](
	[userId] [nvarchar](450) NOT NULL,
	[gameId] [uniqueidentifier] NOT NULL,
	[datePurchased] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[userId] ASC,
	[gameId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WishListItem]    Script Date: 2020-12-08 6:18:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WishListItem](
	[userId] [nvarchar](450) NOT NULL,
	[gameId] [uniqueidentifier] NOT NULL,
	[dateCreated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[userId] ASC,
	[gameId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AddressMailing_CountryCode]    Script Date: 2020-12-08 6:18:51 PM ******/
CREATE NONCLUSTERED INDEX [IX_AddressMailing_CountryCode] ON [dbo].[AddressMailing]
(
	[CountryCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AddressMailing_ProvinceCode]    Script Date: 2020-12-08 6:18:51 PM ******/
CREATE NONCLUSTERED INDEX [IX_AddressMailing_ProvinceCode] ON [dbo].[AddressMailing]
(
	[ProvinceCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AddressMailing_userId]    Script Date: 2020-12-08 6:18:51 PM ******/
CREATE NONCLUSTERED INDEX [IX_AddressMailing_userId] ON [dbo].[AddressMailing]
(
	[userId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AddressShipping_CountryCode]    Script Date: 2020-12-08 6:18:51 PM ******/
CREATE NONCLUSTERED INDEX [IX_AddressShipping_CountryCode] ON [dbo].[AddressShipping]
(
	[CountryCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AddressShipping_ProvinceCode]    Script Date: 2020-12-08 6:18:51 PM ******/
CREATE NONCLUSTERED INDEX [IX_AddressShipping_ProvinceCode] ON [dbo].[AddressShipping]
(
	[ProvinceCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AddressShipping_userId]    Script Date: 2020-12-08 6:18:51 PM ******/
CREATE NONCLUSTERED INDEX [IX_AddressShipping_userId] ON [dbo].[AddressShipping]
(
	[userId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetRoleClaims_RoleId]    Script Date: 2020-12-08 6:18:51 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetRoleClaims_RoleId] ON [dbo].[AspNetRoleClaims]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [RoleNameIndex]    Script Date: 2020-12-08 6:18:51 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[NormalizedName] ASC
)
WHERE ([NormalizedName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserClaims_UserId]    Script Date: 2020-12-08 6:18:51 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserClaims_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserLogins_UserId]    Script Date: 2020-12-08 6:18:51 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserLogins_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserRoles_RoleId]    Script Date: 2020-12-08 6:18:51 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserRoles_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [EmailIndex]    Script Date: 2020-12-08 6:18:51 PM ******/
CREATE NONCLUSTERED INDEX [EmailIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UserNameIndex]    Script Date: 2020-12-08 6:18:51 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedUserName] ASC
)
WHERE ([NormalizedUserName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_CategoryPreference_gamecategoryId]    Script Date: 2020-12-08 6:18:51 PM ******/
CREATE NONCLUSTERED INDEX [IX_CategoryPreference_gamecategoryId] ON [dbo].[CategoryPreference]
(
	[gamecategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Game_EsrbRatingCode]    Script Date: 2020-12-08 6:18:51 PM ******/
CREATE NONCLUSTERED INDEX [IX_Game_EsrbRatingCode] ON [dbo].[Game]
(
	[EsrbRatingCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Game_GameCategoryId]    Script Date: 2020-12-08 6:18:51 PM ******/
CREATE NONCLUSTERED INDEX [IX_Game_GameCategoryId] ON [dbo].[Game]
(
	[GameCategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Game_GamePerspectiveCode]    Script Date: 2020-12-08 6:18:51 PM ******/
CREATE NONCLUSTERED INDEX [IX_Game_GamePerspectiveCode] ON [dbo].[Game]
(
	[GamePerspectiveCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Game_GameStatusCode]    Script Date: 2020-12-08 6:18:51 PM ******/
CREATE NONCLUSTERED INDEX [IX_Game_GameStatusCode] ON [dbo].[Game]
(
	[GameFormatCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Game_GameSubCategoryId]    Script Date: 2020-12-08 6:18:51 PM ******/
CREATE NONCLUSTERED INDEX [IX_Game_GameSubCategoryId] ON [dbo].[Game]
(
	[GameSubCategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_PlatformPreference_platformCode]    Script Date: 2020-12-08 6:18:51 PM ******/
CREATE NONCLUSTERED INDEX [IX_PlatformPreference_platformCode] ON [dbo].[PlatformPreference]
(
	[platformCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_SubCategoryPreference_gameSubcategoryId]    Script Date: 2020-12-08 6:18:51 PM ******/
CREATE NONCLUSTERED INDEX [IX_SubCategoryPreference_gameSubcategoryId] ON [dbo].[SubCategoryPreference]
(
	[gameSubcategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AddressMailing] ADD  DEFAULT (getdate()) FOR [lastModified]
GO
ALTER TABLE [dbo].[AddressShipping] ADD  DEFAULT (getdate()) FOR [lastModified]
GO
ALTER TABLE [dbo].[AspNetUsers] ADD  DEFAULT ((1)) FOR [PromoEmailEnabled]
GO
ALTER TABLE [dbo].[AspNetUsers] ADD  DEFAULT ('Tell us about yourself.') FOR [Bio]
GO
ALTER TABLE [dbo].[AspNetUsers] ADD  DEFAULT ('YourGamerTag') FOR [GamerTag]
GO
ALTER TABLE [dbo].[AspNetUsers] ADD  DEFAULT ('CAN') FOR [countryCode]
GO
ALTER TABLE [dbo].[CartItem] ADD  DEFAULT (getdate()) FOR [lastModified]
GO
ALTER TABLE [dbo].[CategoryPreference] ADD  DEFAULT (getdate()) FOR [lastModified]
GO
ALTER TABLE [dbo].[Game] ADD  DEFAULT ('Unknown') FOR [UserName]
GO
ALTER TABLE [dbo].[Game] ADD  DEFAULT (getdate()) FOR [lastModified]
GO
ALTER TABLE [dbo].[Order] ADD  DEFAULT (getdate()) FOR [dateCreated]
GO
ALTER TABLE [dbo].[OrderItem] ADD  DEFAULT (getdate()) FOR [lastModified]
GO
ALTER TABLE [dbo].[PlatformPreference] ADD  DEFAULT (getdate()) FOR [lastModified]
GO
ALTER TABLE [dbo].[Province] ADD  DEFAULT ((0)) FOR [FederalTax]
GO
ALTER TABLE [dbo].[Province] ADD  DEFAULT ((0)) FOR [ProvincialTax]
GO
ALTER TABLE [dbo].[Province] ADD  DEFAULT ((0)) FOR [PstOnGst]
GO
ALTER TABLE [dbo].[SubCategoryPreference] ADD  DEFAULT (getdate()) FOR [lastModified]
GO
ALTER TABLE [dbo].[UserGameLibraryItem] ADD  DEFAULT (getdate()) FOR [datePurchased]
GO
ALTER TABLE [dbo].[WishListItem] ADD  DEFAULT (getdate()) FOR [dateCreated]
GO
ALTER TABLE [dbo].[AddressMailing]  WITH CHECK ADD  CONSTRAINT [FK__AddressMa__userI__7908F585] FOREIGN KEY([userId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[AddressMailing] CHECK CONSTRAINT [FK__AddressMa__userI__7908F585]
GO
ALTER TABLE [dbo].[AddressMailing]  WITH CHECK ADD  CONSTRAINT [FK_AddressMailing_Country_CountryCode] FOREIGN KEY([CountryCode])
REFERENCES [dbo].[Country] ([Code])
GO
ALTER TABLE [dbo].[AddressMailing] CHECK CONSTRAINT [FK_AddressMailing_Country_CountryCode]
GO
ALTER TABLE [dbo].[AddressMailing]  WITH CHECK ADD  CONSTRAINT [FK_AddressMailing_Province_ProvinceCode] FOREIGN KEY([ProvinceCode])
REFERENCES [dbo].[Province] ([Code])
GO
ALTER TABLE [dbo].[AddressMailing] CHECK CONSTRAINT [FK_AddressMailing_Province_ProvinceCode]
GO
ALTER TABLE [dbo].[AddressShipping]  WITH CHECK ADD  CONSTRAINT [FK__AddressSh__userI__79FD19BE] FOREIGN KEY([userId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[AddressShipping] CHECK CONSTRAINT [FK__AddressSh__userI__79FD19BE]
GO
ALTER TABLE [dbo].[AddressShipping]  WITH CHECK ADD  CONSTRAINT [FK_AddressShipping_Country_CountryCode] FOREIGN KEY([CountryCode])
REFERENCES [dbo].[Country] ([Code])
GO
ALTER TABLE [dbo].[AddressShipping] CHECK CONSTRAINT [FK_AddressShipping_Country_CountryCode]
GO
ALTER TABLE [dbo].[AddressShipping]  WITH CHECK ADD  CONSTRAINT [FK_AddressShipping_Province_ProvinceCode] FOREIGN KEY([ProvinceCode])
REFERENCES [dbo].[Province] ([Code])
GO
ALTER TABLE [dbo].[AddressShipping] CHECK CONSTRAINT [FK_AddressShipping_Province_ProvinceCode]
GO
ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUsers]  WITH CHECK ADD FOREIGN KEY([countryCode])
REFERENCES [dbo].[Country] ([Code])
GO
ALTER TABLE [dbo].[AspNetUsers]  WITH CHECK ADD FOREIGN KEY([provinceCode])
REFERENCES [dbo].[Province] ([Code])
GO
ALTER TABLE [dbo].[AspNetUserTokens]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserTokens] CHECK CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[CartItem]  WITH CHECK ADD FOREIGN KEY([GameFormatCode])
REFERENCES [dbo].[GameFormat] ([Code])
GO
ALTER TABLE [dbo].[CartItem]  WITH CHECK ADD FOREIGN KEY([gameId])
REFERENCES [dbo].[Game] ([Guid])
GO
ALTER TABLE [dbo].[CartItem]  WITH CHECK ADD FOREIGN KEY([userId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[CategoryPreference]  WITH CHECK ADD  CONSTRAINT [FK__CategoryP__gamec__61316BF4] FOREIGN KEY([gamecategoryId])
REFERENCES [dbo].[GameCategory] ([Id])
GO
ALTER TABLE [dbo].[CategoryPreference] CHECK CONSTRAINT [FK__CategoryP__gamec__61316BF4]
GO
ALTER TABLE [dbo].[CategoryPreference]  WITH CHECK ADD  CONSTRAINT [FK__CategoryP__userI__603D47BB] FOREIGN KEY([userId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[CategoryPreference] CHECK CONSTRAINT [FK__CategoryP__userI__603D47BB]
GO
ALTER TABLE [dbo].[Game]  WITH CHECK ADD  CONSTRAINT [FK_Game_Game] FOREIGN KEY([Guid])
REFERENCES [dbo].[Game] ([Guid])
GO
ALTER TABLE [dbo].[Game] CHECK CONSTRAINT [FK_Game_Game]
GO
ALTER TABLE [dbo].[Game]  WITH CHECK ADD  CONSTRAINT [Game_EsrbRating_FK] FOREIGN KEY([EsrbRatingCode])
REFERENCES [dbo].[EsrbRating] ([Code])
GO
ALTER TABLE [dbo].[Game] CHECK CONSTRAINT [Game_EsrbRating_FK]
GO
ALTER TABLE [dbo].[Game]  WITH CHECK ADD  CONSTRAINT [Game_GameCategory_FK] FOREIGN KEY([GameCategoryId])
REFERENCES [dbo].[GameCategory] ([Id])
GO
ALTER TABLE [dbo].[Game] CHECK CONSTRAINT [Game_GameCategory_FK]
GO
ALTER TABLE [dbo].[Game]  WITH CHECK ADD  CONSTRAINT [Game_GameFormat_FK] FOREIGN KEY([GameFormatCode])
REFERENCES [dbo].[GameFormat] ([Code])
GO
ALTER TABLE [dbo].[Game] CHECK CONSTRAINT [Game_GameFormat_FK]
GO
ALTER TABLE [dbo].[Game]  WITH CHECK ADD  CONSTRAINT [Game_GamePerspective_FK] FOREIGN KEY([GamePerspectiveCode])
REFERENCES [dbo].[GamePerspective] ([Code])
GO
ALTER TABLE [dbo].[Game] CHECK CONSTRAINT [Game_GamePerspective_FK]
GO
ALTER TABLE [dbo].[Game]  WITH CHECK ADD  CONSTRAINT [Game_GameSubCategory_FK] FOREIGN KEY([GameSubCategoryId])
REFERENCES [dbo].[GameSubCategory] ([Id])
GO
ALTER TABLE [dbo].[Game] CHECK CONSTRAINT [Game_GameSubCategory_FK]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD FOREIGN KEY([mailingId])
REFERENCES [dbo].[AddressMailing] ([mailingId])
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD FOREIGN KEY([shippingId])
REFERENCES [dbo].[AddressShipping] ([shippingId])
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD FOREIGN KEY([userId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[OrderItem]  WITH CHECK ADD FOREIGN KEY([GameFormatCode])
REFERENCES [dbo].[GameFormat] ([Code])
GO
ALTER TABLE [dbo].[OrderItem]  WITH CHECK ADD FOREIGN KEY([gameId])
REFERENCES [dbo].[Game] ([Guid])
GO
ALTER TABLE [dbo].[OrderItem]  WITH CHECK ADD FOREIGN KEY([orderId])
REFERENCES [dbo].[Order] ([Id])
GO
ALTER TABLE [dbo].[PlatformPreference]  WITH CHECK ADD  CONSTRAINT [FK__PlatformP__platf__5C6CB6D7] FOREIGN KEY([platformCode])
REFERENCES [dbo].[Platform] ([Code])
GO
ALTER TABLE [dbo].[PlatformPreference] CHECK CONSTRAINT [FK__PlatformP__platf__5C6CB6D7]
GO
ALTER TABLE [dbo].[PlatformPreference]  WITH CHECK ADD  CONSTRAINT [FK__PlatformP__userI__5B78929E] FOREIGN KEY([userId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[PlatformPreference] CHECK CONSTRAINT [FK__PlatformP__userI__5B78929E]
GO
ALTER TABLE [dbo].[SubCategoryPreference]  WITH CHECK ADD  CONSTRAINT [FK__SubCatego__gameS__65F62111] FOREIGN KEY([gameSubcategoryId])
REFERENCES [dbo].[GameSubCategory] ([Id])
GO
ALTER TABLE [dbo].[SubCategoryPreference] CHECK CONSTRAINT [FK__SubCatego__gameS__65F62111]
GO
ALTER TABLE [dbo].[SubCategoryPreference]  WITH CHECK ADD  CONSTRAINT [FK__SubCatego__userI__6501FCD8] FOREIGN KEY([userId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[SubCategoryPreference] CHECK CONSTRAINT [FK__SubCatego__userI__6501FCD8]
GO
ALTER TABLE [dbo].[UserGameLibraryItem]  WITH CHECK ADD FOREIGN KEY([gameId])
REFERENCES [dbo].[Game] ([Guid])
GO
ALTER TABLE [dbo].[UserGameLibraryItem]  WITH CHECK ADD FOREIGN KEY([userId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[WishListItem]  WITH CHECK ADD FOREIGN KEY([gameId])
REFERENCES [dbo].[Game] ([Guid])
GO
ALTER TABLE [dbo].[WishListItem]  WITH CHECK ADD FOREIGN KEY([userId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
USE [master]
GO
ALTER DATABASE [CVGS] SET  READ_WRITE 
GO
