## Project Requirements
Visual Studio
ASP .Net Core 2.2
SQL Express Server

## Build Intructions
1. Install the required software listed above.
2. Create the database using the sql scripts in /dbsetup. Run schema.sql first then data.sql
3. Open the project using Visual studio.
4. Edit the connection strings in /CVGS/appsettings.json to match your local db connection string.
5. Build and run using Visual studio.

## License
I chose the Apache License because I want anyone to access and use this project.
